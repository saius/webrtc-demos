// ejemplo traido de:
// https://www.html5rocks.com/en/tutorials/webrtc/infrastructure/#building-a-signaling-service-with-socket.io-on-node

const http = require('http');
const app = http.createServer(function (req, res) {
  console.log('Nueva request')
  // https://www.w3schools.com/nodejs/obj_http_serverresponse.asp
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.write('Servidor de WebSocket!');
  res.end();
}).listen(4000);

const io = require('socket.io')(app);

io.sockets.on('connection', (socket) => {
  console.log('new connection')

  // Convenience function to log server messages to the client
  function log(){
    const array = ['>>> Message from server: '];
    for (const i = 0; i < arguments.length; i++) {
      array.push(arguments[i]);
    }
      socket.emit('log', array);
  }

  socket.on('message', (message) => {
    log('Got message:', message);
    // For a real app, would be room only (not broadcast)
    socket.broadcast.emit('message', message);
  });

  socket.on('create or join', (room) => {
    const numClients = io.sockets.clients(room).length;

    log('Room ' + room + ' has ' + numClients + ' client(s)');
    log('Request to create or join room ' + room);

    if (numClients === 0){
      socket.join(room);
      socket.emit('created', room);
    } else if (numClients === 1) {
      io.sockets.in(room).emit('join', room);
      socket.join(room);
      socket.emit('joined', room);
    } else { // max two clients
      socket.emit('full', room);
    }
    socket.emit('emit(): client ' + socket.id +
      ' joined room ' + room);
    socket.broadcast.emit('broadcast(): client ' + socket.id +
      ' joined room ' + room);

  });

});
