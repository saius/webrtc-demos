# WebRTC

## Demos

* `webrtc-webcam-demo.html` prueba la api básica de webRTC para conectarse a la cámara de la compu y mostrarla en un video HTML5
* `webrtc-nat-type-test.html` comprueba si estás atrás de un NAT/rúter simétrico o uno normal
* `peerjs-demo-peer.html` es una demo de la librería peerjs. Hay que abrir el archivo en dos navegadores
distintos (o compus distintas) e intentar hablarse entre sí pasándose antes sus respectivos IDs

## Documentación oficial WebRTC

Guía oficial https://webrtc.org/getting-started/overview

Samples oficiales https://webrtc.github.io/samples/

## Guías prácticas

* contruir webrtc backend con socket.io https://www.html5rocks.com/en/tutorials/webrtc/infrastructure/

* socket.io One-to-One Video Chat https://www.webrtc-experiment.com/socket.io/#4602758069873752

## Códigos & herramientas web

* __coturn__ [C lang] TURN/STUN server project https://github.com/coturn/coturn (6.4k stars, 1300 commits)

* __peerjs__ PeerJS wraps the browser's WebRTC implementation to provide a complete, configurable, and easy-to-use peer-to-peer connection API. https://peerjs.com/ https://github.com/peers/peerjs (9.2k starts, 928 commits)

* __peerjs-server__  Server for PeerJS https://github.com/peers/peerjs-server (3.1k stars, 307 commits)

* __pion__ [GO lang] Pion TURN, an API for building TURN clients and servers  https://github.com/pion/turn

* WebRTC Demos, Experiments, Libraries, Examples https://www.webrtc-experiment.com/

* servidores públicos stun/turn: https://gist.github.com/sagivo/3a4b2f2c7ac6e1b5267c2f1f59ac6c6b

* ICE candidate gathering test: https://webrtc.github.io/samples/src/content/peerconnection/trickle-ice/

* WebRTC Troubleshooter oficial https://test.webrtc.org/

## Empresas & servicios pagos

* testRTC: Test & monitor Your WEBRTC-Based Application at scale https://testrtc.com/

* SimpleWebRTC: Empowering developers of all skill levels to build advanced realtime apps without breaking the bank https://www.simplewebrtc.com/

* xirsys: Global TURN server infrastructure for powering WebRTC applications and services https://xirsys.com/

* twilio: Global Network Traversal Service. Low-latency, cost-effective, reliable STUN and TURN capabilities for WebRTC, distributed across five continents. https://www.twilio.com/

## Guías teóricas

* Definición STUN https://webrtcglossary.com/stun/

* Intro a STUN vs TURN servers https://webrtc.ventures/2020/12/webrtc-signaling-stun-vs-turn/   
~> "commercial WebRTC platforms": https://www.vonage.com/communications-apis/ y https://www.twilio.com/video   
~> "Microsoft has also launched its own CPaaS, Azure Communication Services"

* Guía genérica tutorial https://bloggeek.me/webrtc-turn/   
~> "Common lore indicates that around 80% of all connections can be resolved (...) by use of STUN and public IP addresses."

* Definición signaling server https://webrtc.ventures/2018/06/all-you-need-is-love-and-webrtc-signaling/

* 5 protocolos de signaling usados en WebRTC https://bloggeek.me/siganling-protocol-webrtc/

* Trickle ICE vs ICE https://webrtcglossary.com/trickle-ice/

## Glosario

* __Symmetric NAT__: "Each request from the same internal IP address and port to a specific destination IP address and port is mapped to a unique external source IP address and port; if the same internal host sends a packet even with the same source address and port but to a different destination, a different mapping is used. Then, only an external host that receives a packet from an internal host can send a packet back." https://en.wikipedia.org/wiki/Network_address_translation#Symmetric_NAT

* __SDP__: "The Session Description Protocol (SDP) is a format for describing multimedia communication sessions for the purposes of announcement and invitation.[1] Its predominant use is in support of streaming media applications, such as voice over IP (VoIP) and video conferencing. SDP does not deliver any media streams itself but is used between endpoints for negotiation of network metrics, media types, and other associated properties. The set of properties and parameters is called a session profile." https://en.wikipedia.org/wiki/Session_Description_Protocol

* __ICE__: "Interactive Connectivity Establishment (ICE): A Protocol for Network Address Translator (NAT) Traversal. Is a technique used in computer networking to find ways for two computers to talk to each other as directly as possible in peer-to-peer networking.", "Since the network conditions can vary dependning on a number of factors, an external service is usually used for discovering the possible candidates for connecting to a peer. This service is called ICE and is using either a STUN or a TURN server." https://en.wikipedia.org/wiki/Interactive_Connectivity_Establishment, https://webrtc.org/getting-started/peer-connections#ice_candidates

* __Trickle ICE__ "is one of the methods for speeding up the ICE process. The idea behind Trickle ICE is that an agent may send and receive ICE candidates incrementally. By doing so, the agent may start connectivity checks before all candidates have been gathered, thus shortening ICE process time." https://webrtcstandards.info/webrtc-trickle-ice/
